# ============================================================================ #
# Build library
# ============================================================================ #
add_library(${library_name}
  src/json_packer/packer.hpp
  src/json_packer/packer.cpp

  src/json_packer/json_parser.cpp
  src/json_packer/json_parser.hpp

  src/json_packer/tlv_encoder.cpp
  src/json_packer/tlv_encoder.hpp

  src/json_packer/containers.hpp

  src/json_packer/json_values/type.hpp
  src/json_packer/json_values/interface.hpp
  src/json_packer/json_values/string_value.hpp
  src/json_packer/json_values/integer_value.hpp
  src/json_packer/json_values/boolean_value.hpp
)

target_link_libraries(${library_name} ${CONAN_TARGETS})

# ============================================================================ #
# Build the executable
# ============================================================================ #
if(${BUILD_EXE})
  message(STATUS "BUILD_EXE was activated, the ${PROJECT_NAME} executable will "
    " be built as well.")
  add_executable(${PROJECT_NAME}
    src/cli.hpp src/cli.cpp
    src/main.cpp)
  # Wait for the library tob finish
  add_dependencies(${PROJECT_NAME} ${library_name})
  target_link_libraries(${PROJECT_NAME} ${library_name} ${CONAN_TARGETS})
else()
  message(STATUS "BUILD_EXE is not activated, the ${PROJECT_NAME} executable will "
    "not be built.")
endif()

# ============================================================================ #
# Build the unit tests executable
# ============================================================================ #
if(${BUILD_UNIT_TESTS})
  message(STATUS "BUILD_UNIT_TESTS was activated, the ${PROJECT_NAME}_tests_* "
    "executables will be built as well.")
  # Due to being my first time with boost test, I couldn't find a way to make it
  # work with only one executable.
  add_executable(${PROJECT_NAME}_tests_json_parser
    src/test/json_packer/json_parser.hpp
    src/test/json_packer/json_parser.cpp

    src/test/utils.hpp
    src/test/utils.cpp
  )

  add_executable(${PROJECT_NAME}_tests_tlv_encoder
    src/test/json_packer/tlv_encoder.hpp
    src/test/json_packer/tlv_encoder.cpp

    src/test/utils.hpp
    src/test/utils.cpp
  )
  add_executable(${PROJECT_NAME}_tests_packer
    src/test/json_packer/packer.hpp
    src/test/json_packer/packer.cpp

    src/test/utils.hpp
    src/test/utils.cpp
  )

  # Wait for the library tob finish
  add_dependencies(${PROJECT_NAME}_tests_json_parser ${library_name})
  add_dependencies(${PROJECT_NAME}_tests_tlv_encoder ${library_name})
  add_dependencies(${PROJECT_NAME}_tests_packer ${library_name})

  target_link_libraries(${PROJECT_NAME}_tests_json_parser ${library_name} ${CONAN_TARGETS})
  target_link_libraries(${PROJECT_NAME}_tests_tlv_encoder ${library_name} ${CONAN_TARGETS})
  target_link_libraries(${PROJECT_NAME}_tests_packer ${library_name} ${CONAN_TARGETS})
else()
  message(STATUS "BUILD_UNIT_TESTS is not activated, the ${PROJECT_NAME}_tests "
    "executable will not be built.")
endif()
