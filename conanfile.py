from conans import ConanFile, CMake
# Will be used to disable unwanted boost libraries
BOOST_LIBRARIES_NOT_USED = (
  "context",
  "contract",
  "coroutine",
  "fiber",
  "graph",
  "graph_parallel",
  "iostreams",
  "locale",
  "math",
  "mpi",
  "nowide",
  "python",
  "serialization",
  "stacktrace",
  "timer",
  "type_erasure",
  "wave",
)

class JsonPacker(ConanFile):
  name = "json_packer"
  settings = "os", "compiler", "build_type", "arch"
  requires = "boost/1.78.0"
  generators = "cmake"
  default_options = {"boost:without_json": False, "boost:without_log": False}
  default_options.update({"boost:without_{}".format(_name): True for _name in BOOST_LIBRARIES_NOT_USED})

  # environment variable to build the unit tests executable
  BUILD_UNIT_TESTS = "BUILD_UNIT_TESTS"

  # environment variable to build the executable
  BUILD_EXE = "BUILD_EXE"

  def configure(self):
    # TODO: fix this, atm this gets built even when the BUILD_UNIT_TESTS is not
    # passed. Conan might provide another method where to change the options.
    if self.env.get(self.BUILD_UNIT_TESTS, None) == "True":
      self.default_options.update({"boost:without_test": False})
    else:
      self.default_options.update({"boost:without_test": True})

  def build(self):
    cmake = CMake(self)
    if self.env.get(self.BUILD_UNIT_TESTS, None) == "True":
      cmake.definitions[self.BUILD_UNIT_TESTS] = True
    if self.env.get(self.BUILD_EXE, None) == "False":
      cmake.definitions[self.BUILD_EXE] = False
    cmake.configure()
    cmake.build()
