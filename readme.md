# JSON_PACKER

This tool parses a json file that contains one json object per line and genereates two output files from that using tlv encoding:

- values encoded file
- keys encoded file

After passing the input file via: `-I [ --input-json-file ]` arg, the tool will process and generate those two files with the following names:

- values encoded file: `input-json-file`+`_values_tlv_encoded`
- keys encoded file: `input-json-file`+`_keys_tlv_encoded`

The output format of those files is the following:

The values will be encoded as it follows:

- tag - 1 byte - Which can be one of the values from the `encoding_tag` enum
- length - 1 byte for Boolean and Integer or 4 bytes(uint32) for String. This contains the length of the remaining bytes from this value, which represents: key + value.
- key - 4 bytes(uint32), this is the numeric key that has been allocated for the string key of this value
- value(generic: length - 4 bytes(key size)):
  - 1 byte for Boolean
  - 8 bytes for Integer(int64_t)
  - X bytes for String
- The end of a json object will be marked with `encoding_tag::EntryEnd`.

The keys will be encoded as it follows:

- tag - 1 byte - Which can be one of the following values from the `encoding_tag` enum: `Key` and `EntryEnd`.
- length - 4 bytes(uint32). This contains the length of the remaining bytes from this key, which represents: key + string value.
- key - 4 bytes(uint32), this is the numeric key that has been allocated forthe string key of this value
- value(generic: length - 4 bytes(key size)) - Contains the string representation of the numeric key. The end of a json object will be marked with `EntryEnd`.

**Note: If you want the keys map to persist between lines, you should pass `-p [ --persistent-keys ]` arg, this will result in havin only a single entry in the keys output file.**

All available options:

```bash
Generic options:
  -h [ --help ]                  Show all allowed options

Options:
  -I [ --input-json-file ] arg   The json file that we want to pack.
  -p [ --persistent-keys ]       Do not clear the keys map for each line from 
                                 the input file.

Configuration:
  --log-level arg (=info)        Set log level: trace, info, error
  -s [ --chunk-size ] arg (=10)  The size of each chunk read from the input 
                                 file, we will read the file in chunks.
```

## Requirements

- [conan](https://conan.io/downloads.html) >= 1.46.2 - As package manager
- cmake >= 3.5
- gcc >= 9.4.0 - As compiler

*Optional:* [QtCreator](https://www.qt.io/product/development-tools)

*Note: If using QtCreator, you can edit the build configuration and add a custom build step instead of the default one. You can set the command to `conan` and arguments to `build %{sourceDir}`.*

### Installing requirements

```bash
# Make sure pip is using python3
pip install conan

# Cmake
sudo apt-get install cmake

# gcc
sudo apt-get install build-essential
```

## Dependencies

*These are resolved by conan.*

- boost - 1.78.0
  - json
  - log
  - program_options
  - test - only for unit tests
  - and the dependencies of those components

## Building json_packer

```bash
mkdir build && cd build

conan install .. -s json_packer:build_type=Debug --build=missing # Boost might need to be build to work

conan build ..
```

If you also want to build the unit tests executables run:

**Note: Due to working the first time with boost.test(I've been working with gtest until now), I couldn't find a way to make it work with only one executable.**

```bash
conan install .. -s json_packer:build_type=Debug  -e BUILD_UNIT_TESTS="True" --build=missing # Boost might need to be build to work

conan build ..

# Run tests
./json_packer_tests_tests_json_parser --log_level=unit_scope
./json_packer_tests_tests_tlv_encoder --log_level=unit_scope
./json_packer_tests_tests_packer --log_level=unit_scope
```

## Improvements

- [ ] Error codes + error messages
- [ ] Wrapper for working with the file, to inject and mock working with files.
- [ ] Skip error lines?
- [ ] Encoding, little endian(least semnificative bit first) vs Big endian
- [ ] Pass the output filenames via command args
- [ ] Read from cin, to be able to pipe output from other tool: `echo "json" | json_packer`
- [ ] Multiple test scenarios
- [ ] Boost tests fix, only one executable
