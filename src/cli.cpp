#include "cli.hpp"

#include <iostream>

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

namespace po = boost::program_options;
using namespace std;

namespace json_packer
{
//----------------------------------------------------------------------------//
void cli::initialize( int argc, char* argv[] )
{
  po::options_description generic( "Generic options" );
  generic.add_options()( "help,h", "Show all allowed options" );

  po::options_description input( "Options" );
  input.add_options()( "input-json-file,I",
    po::value<string>( &input_json_file ),
    "The json file that we want to pack." )( "persistent-keys,p",
    "Do not clear the keys map for each line from the input file." );

  po::options_description config( "Configuration" );
  config.add_options()( "log-level",
    po::value<string>( &log_level )->default_value( "info" ),
    "Set log level: trace, info, error" )( "chunk-size,s",
    po::value<size_t>( &chunk_size )->default_value( 10 ),
    "The size of each chunk read from the input file, we will read the file in "
    "chunks." );

  cmdline_options.add( generic ).add( input ).add( config );

  po::store( po::parse_command_line( argc, argv, cmdline_options ), vm );
  po::notify( vm );
}
//----------------------------------------------------------------------------//
void cli::validate()
{
  if ( vm.count( "help" ) )
  {
    cout << cmdline_options << "\n";
    exit( 1 );
  }

  // Validate args
  if ( input_json_file.empty() )
  {
    cout << "input-json-file arg not provided." << endl;
    exit( 1 );
  }

  set_log_level();

  if ( vm.count( "persistent-keys" ) )
  {
    persistent_keys = true;
  }
}
//----------------------------------------------------------------------------//
void cli::set_log_level() const noexcept
{

  if ( log_level == "trace" )
  {
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::trace );
  }
  else if ( log_level == "debug" )
  {
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::debug );
  }
  else if ( log_level == "error" )
  {
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::error );
  }
  else
  {
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::info );
  }
}
//----------------------------------------------------------------------------//
} // namespace json_packer
