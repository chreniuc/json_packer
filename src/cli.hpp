#ifndef SRC_CLI_HPP
#define SRC_CLI_HPP

#include <string>

#include <boost/program_options.hpp>

namespace json_packer
{
/**
 * @brief The cli class is used to create the supported args by the executable
 * and also validate them
 */
class cli
{
  /**
   * @brief log_level log level to be used.
   */
  std::string log_level{ "info" };

  /**
   * @brief vm Stores the args.
   */
  boost::program_options::variables_map vm;

  /**
   * @brief cmdline_options Supported args, used to print them when `help` is
   * given.
   */
  boost::program_options::options_description cmdline_options;

public:
  /**
   * @brief input_json_file Input json file.
   */
  std::string input_json_file;
  /**
   * @brief chunk_size Chunk size of bytes to read from the input file in one
   * read.
   */
  size_t chunk_size{ 10 };

  /**
   * @brief persistent_keys If this is passed, it means that we will use the
   * same keys map fopr all entries from the json input file. So at the end we
   * will write only once in the keys output file.
   */
  bool persistent_keys{ false };

  cli() = default;

  /**
   * @brief initialize Will set the supported args and their docs.
   * @param[in] argc - Number of args.
   * @param[in] argv - Arguments list from cmd.
   */
  void initialize( int argc, char* argv[] );

  /**
   * @brief validate Validates the input args, if something is wrong, it will
   * exit the process.
   */
  void validate();

private:
  /**
   * @brief set_log_level Set the log level for boost trivial logger, based on
   * the config param: log-level.
   */
  void set_log_level() const noexcept;
};
} // namespace json_packer

#endif // SRC_CLI_HPP
