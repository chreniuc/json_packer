#ifndef SRC_JSON_PACKER_CONTAINERS_HPP
#define SRC_JSON_PACKER_CONTAINERS_HPP

#include <cstdint>
#include <map>
#include <memory>
#include <string>
//#include <unordered_map>

#include "json_packer/json_values/interface.hpp" // json_value_ptr

namespace json_packer
{
using numeric_key = uint32_t;

using keys_map = ::std::map<std::string, numeric_key>;
// or
// using keys_map = ::std::unordered_map<std::string, numeric_key>;

using values_map = ::std::map<numeric_key, json_value_ptr>;
// or
// using values_map = ::std::unordered_map<numeric_key, json_value_ptr>;
} // namespace json_packer
#endif // SRC_JSON_PACKER_CONTAINERS_HPP
