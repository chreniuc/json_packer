#include "json_packer/json_parser.hpp"

#include <iostream>
#include <memory>

#include <boost/json.hpp>
#include <boost/log/trivial.hpp>

#include <json_packer/json_values/boolean_value.hpp>
#include <json_packer/json_values/integer_value.hpp>
#include <json_packer/json_values/string_value.hpp>

using namespace ::std;

namespace json_packer
{
//----------------------------------------------------------------------------//
json_parser::json_parser(
  const string& input_file_name, const size_t chunk_size )
  : input_file_name( input_file_name ), chunk_size( chunk_size )
{
  // allocate buffer
  buffer.resize( chunk_size );
}
//----------------------------------------------------------------------------//
const string& json_parser::get_error_message() const noexcept
{
  return error_message;
}
//----------------------------------------------------------------------------//
bool json_parser::parse_line( keys_map& keys, values_map& values ) noexcept
{
  BOOST_LOG_TRIVIAL( trace ) << "json_parser::parse_line called";

  if ( !error_message.empty() )
  {
    // There has been an error previously, do not allow reussage of this object.
    return false;
  }

  try
  {
    boost::system::error_code stream_parser_error_code;
    boost::json::stream_parser stream_parser;
    bool stream_parser_contains_data{ false };
    bool found_new_line{ false };
    do
    {
      if ( bytes_read == 0 ||
        current_line_begin_index == 0 || // Partial json object
        current_line_begin_index ==
          bytes_read ) // This happens when we detect a new line and increment
                       // the index. It might happen that that '\n' is the last
                       // from the buffer
      {
        // We need to read the first/next chunk of bytes
        if ( !read_new_chunk() )
        {
          // Error while trying to read
          return false;
        }
        else
        {
          if ( bytes_read == 0 && stream_parser_contains_data )
          {
            // We got to the end of the file but maybe we didn't encounter a
            // '\n' for the previous json object, so we need to see if we need
            // to call finish on the parse method
            return found_complete_json_object( stream_parser, keys, values );
          }
        }
      }

      const long buffer_last_index = bytes_read - 1;
      long current_line_end_index{ current_line_begin_index };
      while ( current_line_end_index <= buffer_last_index )
      {
        if ( buffer[ current_line_end_index ] == '\n' )
        {
          found_new_line = true;
          break;
        }
        current_line_end_index++;
      }

      // Parse only the bytes from one line, do not go over to the next line
      long num_bytes{ current_line_end_index - current_line_begin_index };
      stream_parser_contains_data = true;
      stream_parser.write( buffer.data() + current_line_begin_index, num_bytes,
        stream_parser_error_code );
      if ( stream_parser_error_code )
      {
        error_message = "Error while calling stream_parser.write: " +
          stream_parser_error_code.message() + ". We will ignore this line.";
        return false;
      }
      if ( found_new_line || eof() )
      {
        // We found a new line or we got to the end of the file.
        // The next line will start from the next possition
        current_line_begin_index = current_line_end_index + 1;
        return found_complete_json_object( stream_parser, keys, values );
      }
      else
      {
        // We got to the end of the buffer, we should force another read.
        current_line_begin_index = bytes_read;
      }
    } while ( !eof() );

    return true;
  }
  catch ( exception& e )
  {
    error_message =
      "Exception encountered while trying to parse a line: "s + e.what();
  }
  catch ( ... )
  {
    error_message =
      "Unknown exception encountered while trying to parse a line";
  }
  return false;
}
//----------------------------------------------------------------------------//
bool json_parser::done() const noexcept
{
  // We are done when we got to the end of the current buffer and also at the
  // end of the file or we encountered an error
  return ( current_line_begin_index >= bytes_read && eof() ) ||
    !error_message.empty();
}
//----------------------------------------------------------------------------//
bool json_parser::populate_key_values_maps(
  const boost::json::value& json, keys_map& keys, values_map& values )
{
  if ( !json.is_object() )
  {
    error_message = "Expected valid json object.";
    return false;
  }

  auto const& json_object = json.get_object();
  for ( auto json_iterator = json_object.begin();
        json_iterator != json_object.end(); json_iterator++ )
  {
    const auto& json_element = *json_iterator;
    const auto& value = json_element.value();
    const auto& key = static_cast<string>( json_element.key() );

    auto const result = keys.find( key );

    numeric_key new_numeric_key{ static_cast<numeric_key>( keys.size() + 1 ) };

    if ( result == keys.end() )
    {
      keys.insert( { key, new_numeric_key } );
    }
    else
    {
      new_numeric_key = result->second;
    }

    switch ( value.kind() )
    {
    case boost::json::kind::string:
    {
      values.insert( { new_numeric_key,
        make_shared<json_values::string_value>(
          static_cast<string>( value.as_string() ) ) } );
      break;
    }
    case boost::json::kind::int64:
    {
      values.insert( { new_numeric_key,
        make_shared<json_values::integer_value>( value.as_int64() ) } );
      break;
    }
    case boost::json::kind::bool_:
    {
      values.insert( { new_numeric_key,
        make_shared<json_values::boolean_value>( value.as_bool() ) } );
      break;
    }
    default:
    {
      error_message = "Unsupported json value type.";
      return false;
    }
    }
  }
  return true;
}
//----------------------------------------------------------------------------//
bool json_parser::eof() const noexcept
{
  return end_of_file;
}
//----------------------------------------------------------------------------//
bool json_packer::json_parser::found_complete_json_object(
  boost::json::stream_parser& stream_parser, keys_map& keys,
  values_map& values )
{
  boost::system::error_code stream_parser_error_code;

  // We found a new line or we got to the end of the file.
  stream_parser.finish( stream_parser_error_code );
  if ( stream_parser_error_code )
  {
    error_message = "Error while calling stream_parser.finish: " +
      stream_parser_error_code.message() + ". We will ignore this line.";
    return false;
  }

  auto const json_result = stream_parser.release();
  stream_parser.reset();
  if ( !populate_key_values_maps( json_result, keys, values ) )
  {
    return false;
  }
  return true;
}
//----------------------------------------------------------------------------//
bool json_parser::read_new_chunk()
{
  if ( !infile.is_open() )
  {
    // First time, we should open the file
    infile.open( input_file_name ); // read only mode
    if ( !infile.is_open() )
    {
      error_message = "Couldn't open input file: " + input_file_name;
      return false;
    }
  }

  // Read first chunck of bytes from the input file
  bytes_read = infile.readsome( buffer.data(), chunk_size );

  if ( bytes_read == 0 ) // eof
  {
    end_of_file = true;
    infile.close();
    return true;
  }
  else if ( bytes_read < 0 )
  {
    error_message =
      "Encountered an unexpected error while trying to read from the file: " +
      input_file_name;
    infile.close();
    return false;
  }
  else if ( static_cast<unsigned long>( bytes_read ) < chunk_size )
  {
    // End of file, we couldn't read as much as we wanted, it means that we got
    // to the end.
    end_of_file = true;
    infile.close();
  }
  else
  {
    end_of_file = infile.eof();
  }
  current_line_begin_index = 0;
  return true;
}
//----------------------------------------------------------------------------//
} // namespace json_packer
