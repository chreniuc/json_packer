#ifndef SRC_JSON_PACKER_JSON_PARSER_HPP
#define SRC_JSON_PACKER_JSON_PARSER_HPP

#include <fstream>
#include <string>

#include <boost/json.hpp>

#include <json_packer/containers.hpp> // values_map, keys_map

namespace json_packer
{
/**
 * @brief The json_parser class is used to read the json data from a file, in
 * chunks, and parse it step by step providing two maps to the user:
 *  - One map containing all keys from the json mapped to an integer
 *  - One map containing all values from the json, but the keys being replaced
 * with integers from the previous map.
 *
 */
class json_parser
{
  /**
   * @brief error_message This will be populated if there has been an error
   * while parsing the input file.
   *
   * If the parse_line method returns false, the user should call
   * get_error_message() method, to detect the error.
   *
   * @warning If this is populated, the parsing can no longer continue, so we
   * will not allow aditional calls to parse_line.
   */
  std::string error_message;

  /**
   * @brief input_file_name The na
   */
  const std::string input_file_name;

  /**
   * @brief chunk_size Number of bytes that will be read from the file on every
   * read operation. We are reading in chunks, to avoid reading a whole line at
   * once, because there might be big lines.
   */
  const size_t chunk_size;

  /**
   * @brief infile JSON input file from where we will read chunks of data and
   * parse each json object found on each line.
   *
   * @note This will be open at the first parse call and will be closed when we
   * get to the end of the file or when we encountered an error or when this
   * object is dealocated.
   */
  std::ifstream infile;

  // Streaming data
  /**
   * @brief buffer Buffer for reading the chunks from the JSON file. We will
   * call resize on it with the size of chunk_size in our constructor.
   */
  std::string buffer;
  /**
   * @brief bytes_read Number of bytes read from the file in a single read
   * operation. Reading in chuncks, means that we might read the end of a line
   * and the begining of another line at the same time, so we need to keep track
   * of how many bytes did we read. And also sometimes we might not read
   * chunk_size, because we got to the end of the file.
   */
  long bytes_read{ 0 };
  /**
   * @brief current_line_begin_index Keeps the index of the begining of the
   * current line relative to the buffer. This is usefull when we read the end
   * of a line and the begining of another line at the same time. Which means
   * that we parse only one line and for the other we set this value and
   * continue at the next parse_line call.
   */
  long current_line_begin_index{ 0 };

  /**
   * @brief end_of_file Determines when the end of the file has been achieved.
   */
  bool end_of_file{ true };

public:
  /**
   * @brief json_parser Constructor. Allocates the internal buffer, sets the
   * chunk_size and saves the input file name.
   * @param[in] input_file_name - Input json file.
   * @param[in] chunk_size - Size of a chunck of bytes to be read from the file.
   * We will read it in chunks, not the whole line at once or the whole file.
   *
   * @warning It does not open the input file, that is done in the first
   * parse_line call.
   */
  json_parser(
    const ::std::string& input_file_name, const size_t chunk_size = 10 );

  // Getters
  /**
   * @brief get_error_message Getter for error message, this is set by the
   * parse_line method when an error is encountered.
   * @return Error message.
   */
  const std::string& get_error_message() const noexcept;

  bool parse_line( keys_map& keys, values_map& values ) noexcept;

  /**
   * @brief done Notifies the user when the parsing of the whole file has
   * finished.
   * @return True if the parsing has finished.
   */
  bool done() const noexcept;

private:
  /**
   * @brief populate_key_values_maps Populates the maps based on the values from
   * the json.
   *
   *  - It will map the string keys to integers keys and will add them to the
   * keys map.
   *  - It will map the values to the new integer keys and add them to the
   * values map
   *
   * @note keys is not cleared at the begining, which means that we support
   * appending to existing keys.
   *
   * @param[in] json - Parsed json object.
   * @param[in,out] keys - Keys map, will be populated with the existing keys
   * from the json, that will be mapped to an integer value.
   * @param[out] values - Values map, will be populated with the values from
   * json, these values will contain the numeric key.
   * @return True if everithing went smooth, false otherwise(eg: unsupported
   * json value).
   */
  bool populate_key_values_maps(
    const ::boost::json::value& json, keys_map& keys, values_map& values );

  /**
   * @brief read_new_chunk Read a chunk of bytes(of size: chunk_size) in the
   * buffer. This method will also open the file if it's the first read
   * operation and close it if we got to the end or an error ocurred.
   *
   * @note If there has been an error, the error_message will be set.
   * @return True if the read operation went ok, otherwise false.
   */
  bool read_new_chunk();

  /**
   * @brief eof Checks if we got to the end of the file.
   * @return True if we got to the end of the file.
   */
  bool eof() const noexcept;

  /**
   * @brief found_complete_json_object Called when we detected a complete json
   * object, this will call the json parser.finish and populate_key_values_maps
   * method.
   * @param[in,out] stream_parser - JSON parser.
   * @param[in,out] keys - Keys map, will be populated with the existing keys
   * from the json, that will be mapped to an integer value.
   * @param[out] values - Values map, will be populated with the values from
   * json, these values will contain the numeric key.
   * @return True if everything went ok, false otherwise.
   */
  bool found_complete_json_object( boost::json::stream_parser& stream_parser,
    keys_map& keys, values_map& values );

  // Restricted behaviours
  // Atm I see no reason to have the copy constructors.
  json_parser() = delete;
  json_parser( const json_parser& other ) = delete;
  json_parser& operator=( const json_parser& other ) = delete;
};
} // namespace json_packer
#endif // SRC_JSON_PACKER_JSON_PARSER_HPP
