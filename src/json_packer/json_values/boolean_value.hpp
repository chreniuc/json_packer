#ifndef SRC_JSON_PACKER_JSON_VALUES_BOOLEAN_VALUE_HPP
#define SRC_JSON_PACKER_JSON_VALUES_BOOLEAN_VALUE_HPP

#include "json_packer/json_values/interface.hpp"

namespace json_packer::json_values
{
/**
 * @brief boolean_value Boolean json value.
 */
class boolean_value : public interface
{
public:
  const bool value;
  boolean_value( const bool& value ) : value( value )
  {
  }
  virtual type get_type() const noexcept override
  {
    return type::Boolean;
  }
};
} // namespace json_packer::json_values

#endif // SRC_JSON_PACKER_JSON_VALUES_BOOLEAN_VALUE_HPP
