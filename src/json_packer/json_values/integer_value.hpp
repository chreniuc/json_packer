#ifndef SRC_JSON_PACKER_JSON_VALUES_INTEGER_VALUE_HPP
#define SRC_JSON_PACKER_JSON_VALUES_INTEGER_VALUE_HPP

#include "json_packer/json_values/interface.hpp"

namespace json_packer::json_values
{
/**
 * @brief integer_value Integer json value.
 */
class integer_value : public interface
{
public:
  const int64_t value;
  integer_value( const int64_t& value ) : value( value )
  {
  }
  virtual type get_type() const noexcept override
  {
    return type::Integer;
  }
};
} // namespace json_packer::json_values

#endif // SRC_JSON_PACKER_JSON_VALUES_INTEGER_VALUE_HPP
