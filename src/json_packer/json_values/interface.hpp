#ifndef SRC_JSON_PACKER_JSON_VALUES_INTERFACE_HPP
#define SRC_JSON_PACKER_JSON_VALUES_INTERFACE_HPP

#include <memory>

#include "json_packer/json_values/type.hpp"

namespace json_packer
{
namespace json_values
{
/**
 * @brief interface Interface for all possible json values.
 */
class interface
{
public:
  /**
   * @brief get_type Type of the value.
   * @return Return the type.
   */
  virtual type get_type() const noexcept = 0;
  virtual ~interface() = default;
};
} // namespace json_values
using json_value_ptr = ::std::shared_ptr<json_values::interface>;
} // namespace json_packer

#endif // SRC_JSON_PACKER_JSON_VALUES_INTERFACE_HPP
