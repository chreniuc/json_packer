#ifndef SRC_JSON_PACKER_JSON_VALUES_STRING_VALUE_HPP
#define SRC_JSON_PACKER_JSON_VALUES_STRING_VALUE_HPP

#include <string>

#include "json_packer/json_values/interface.hpp"

namespace json_packer::json_values
{
/**
 * @brief string_value String json value.
 */
class string_value : public interface
{
public:
  const ::std::string value;
  string_value( const ::std::string& value ) : value( value )
  {
  }
  virtual type get_type() const noexcept override
  {
    return type::String;
  }
};
} // namespace json_packer::json_values

#endif // SRC_JSON_PACKER_JSON_VALUES_STRING_VALUE_HPP
