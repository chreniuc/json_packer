#ifndef SRC_JSON_PACKER_JSON_VALUES_TYPE_HPP
#define SRC_JSON_PACKER_JSON_VALUES_TYPE_HPP

namespace json_packer::json_values
{
/**
 * @brief type All supported types of json values
 */
enum type
{
  Integer = 0,
  String,
  Boolean
};
} // namespace json_packer::json_values

#endif // SRC_JSON_PACKER_JSON_VALUES_TYPE_HPP
