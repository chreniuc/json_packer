#include "json_packer/packer.hpp"

#include <filesystem>

#include <boost/log/trivial.hpp>

#include <json_packer/containers.hpp>
#include <json_packer/json_parser.hpp>
#include <json_packer/tlv_encoder.hpp>

using namespace std;

namespace json_packer
{
//----------------------------------------------------------------------------//
bool packer::pack_json( const std::string& input_file_name,
  const size_t chunk_size, const bool persistent_keys,
  const std::string& encoded_keys_output_file_name,
  const std::string& encoded_values_output_file_name ) const noexcept
{
  try
  {
    BOOST_LOG_TRIVIAL( trace ) << "packer::pack_json called";
    if ( input_file_name.empty() )
    {
      BOOST_LOG_TRIVIAL( error ) << "Input file name empty.";
      return false;
    }

    if ( !filesystem::exists( input_file_name ) )
    {
      BOOST_LOG_TRIVIAL( error )
        << "Input file doesn't exist: " << input_file_name;
      return false;
    }

    if ( !filesystem::is_regular_file( input_file_name ) )
    {
      BOOST_LOG_TRIVIAL( error )
        << "Input file is not a regular file, it might "
           "be a directory or sym link(or other): "
        << input_file_name;
      return false;
    }

    BOOST_LOG_TRIVIAL( info )
      << "Preparing to pack file: " << input_file_name << ", which has "
      << filesystem::file_size( input_file_name ) << " bytes.";

    auto const tlv_encoded_keys_output_file_name =
      !encoded_keys_output_file_name.empty()
      ? input_file_name
      : input_file_name + "_keys_tlv_encoded";

    auto const tlv_encoded_values_output_file_name =
      !encoded_values_output_file_name.empty()
      ? input_file_name
      : input_file_name + "_values_tlv_encoded";

    BOOST_LOG_TRIVIAL( info ) << "Output file for encoded keys will be: "
                              << tlv_encoded_keys_output_file_name;
    BOOST_LOG_TRIVIAL( info ) << "Output file for encoded values will be: "
                              << tlv_encoded_values_output_file_name;

    json_packer::json_parser json_parser( input_file_name, chunk_size );
    json_packer::tlv_encoder encoder;

    json_packer::keys_map keys;
    json_packer::values_map values;
    do
    {
      if ( !persistent_keys )
      {
        keys.clear();
      }

      values.clear();
      auto const result = json_parser.parse_line( keys, values );

      if ( !result )
      {
        BOOST_LOG_TRIVIAL( error ) << json_parser.get_error_message();
        return false;
      }

      BOOST_LOG_TRIVIAL( debug ) << "Encoding values map: ";
      if ( !encoder.write_values_map_to_file(
             values, tlv_encoded_values_output_file_name ) )
      {
        BOOST_LOG_TRIVIAL( error )
          << "Error while trying to encode the values map: "
          << encoder.get_error_message();
        return false;
      }
      if ( !persistent_keys )
      {
        BOOST_LOG_TRIVIAL( debug ) << "Encoding keys map: ";
        if ( !encoder.write_keys_map_to_file(
               keys, tlv_encoded_keys_output_file_name ) )
        {
          BOOST_LOG_TRIVIAL( error )
            << "Error while trying to encode the keys map: "
            << encoder.get_error_message();
          return false;
        }
      }
    } while ( !json_parser.done() );

    if ( persistent_keys )
    {
      BOOST_LOG_TRIVIAL( debug ) << "Encoding keys map: ";
      if ( !encoder.write_keys_map_to_file(
             keys, tlv_encoded_keys_output_file_name ) )
      {
        BOOST_LOG_TRIVIAL( error )
          << "Error while trying to encode the keys map: "
          << encoder.get_error_message();
        return false;
      }
    }

    BOOST_LOG_TRIVIAL( info )
      << "Output file for encoded keys generated in: "
      << tlv_encoded_keys_output_file_name << ", contains "
      << filesystem::file_size( tlv_encoded_keys_output_file_name )
      << " bytes.";

    BOOST_LOG_TRIVIAL( info )
      << "Output file for encoded values generated in: "
      << tlv_encoded_values_output_file_name << ", contains "
      << filesystem::file_size( tlv_encoded_values_output_file_name )
      << " bytes.";

    return true;
  }
  catch ( exception& e )
  {
    BOOST_LOG_TRIVIAL( error )
      << "Error while trying to pack the json, exception thrown: " << e.what();
  }
  catch ( ... )
  {
    BOOST_LOG_TRIVIAL( error )
      << "Error while trying to pack the json, unknown exception thrown";
  }
  return false;
}
//----------------------------------------------------------------------------//
} // namespace json_packer
