#ifndef SRC_JSON_PACKER_PACKER_HPP
#define SRC_JSON_PACKER_PACKER_HPP

#include <string>

namespace json_packer
{
/**
 * @brief The packer class is used to combine the other two classes: json_parser
 * and tlv_encoder. We will read line by line from the input file and encode the
 * valuse and write them to the output files.
 */
class packer
{
public:
  packer() = default;

  /**
   * @brief pack_json Will use the json_parser to read the input file line by
   * line and the tlv_encoder to write the values encoded in the output file.
   * @param[in] input_file_name - Input json file.
   * @param[in] chunk_size - Number of bytes to be read in a one time read
   * operation.
   * @param[in] persistent_keys - If true, we will keep the same keys map
   * between json lines. Which means that if a key repeats in multiple json
   * objects, we will have only one entry of it in the keys output file.
   * @param[in] encoded_keys_output_file_name - Output file name for encoded
   * keys. If empty, it will be set to: input_file_name + _keys_tlv_encoded
   * @param[in] encoded_values_output_file_name - Output file name for encoded
   * values. If empty, it will be set to: input_file_name + _values_tlv_encoded
   * @return True if everything went ok, false otherwise + it will log on
   * console the error.
   */
  bool pack_json( const ::std::string& input_file_name, const size_t chunk_size,
    const bool persistent_keys,
    const ::std::string& encoded_keys_output_file_name = "",
    const ::std::string& encoded_values_output_file_name = "" ) const noexcept;
};

} // namespace json_packer
#endif // SRC_JSON_PACKER_PACKER_HPP
