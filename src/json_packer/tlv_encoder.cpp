#include "json_packer/tlv_encoder.hpp"

#include <bitset>
#include <fstream>
#include <iostream>

#include <boost/log/trivial.hpp>

#include "json_packer/json_values/boolean_value.hpp"
#include "json_packer/json_values/integer_value.hpp"
#include "json_packer/json_values/string_value.hpp"

using namespace ::std;

namespace json_packer
{
//----------------------------------------------------------------------------//
bool tlv_encoder::write_values_map_to_file(
  values_map& values, const ::std::string& output_file_name )
{
  BOOST_LOG_TRIVIAL( trace ) << "tlv_encoder::write_values_map_to_file called";
  error_message = "";
  if ( values.empty() )
  {
    error_message = "Empty map";
    return false;
  }
  ofstream outfile;
  outfile.open( output_file_name, ios::binary | ios::out | ios::app );
  if ( !outfile.is_open() )
  {
    error_message =
      "Cant open file to write TLV encoded json: " + output_file_name;
    return false;
  }

  for ( auto const& [ key, value ] : values )
  {
    switch ( value->get_type() )
    {
    case json_values::type::Boolean:
    {
      // Encode the Boolean as it follows:
      //  - first byte is the tag
      //  - second byte is the length - this is the length of the key + the
      //   value
      //  - Next 4 bytes are for the key(uint32_t)
      //  - Next 1 byte is for the value(bool)
      auto const boolean_value =
        dynamic_pointer_cast<json_values::boolean_value>( value );
      // tag
      auto const tag = encoding_tag::Boolean;
      outfile.write( reinterpret_cast<const char*>( &tag ), 1 );
      // Length
      auto const length = sizeof( numeric_key ) + sizeof( bool );
      outfile.write( reinterpret_cast<const char*>( &length ), 1 );
      // Value
      //  - Key
      outfile.write(
        reinterpret_cast<const char*>( &key ), sizeof( numeric_key ) );
      //  - Value
      outfile.write( reinterpret_cast<const char*>( &boolean_value->value ),
        sizeof( bool ) );
      BOOST_LOG_TRIVIAL( debug )
        << "Bool written to file: tag=" << encoding_tag::Boolean
        << ", length=" << length << ", key=" << key
        << ", value=" << boolean_value->value;
      break;
    }
    case json_values::type::Integer:
    {
      // Encode the Integer as it follows:
      //  - first byte is the tag
      //  - second byte is the length - this is the length of the key + the
      //   value
      //  - Next 4 bytes are for the key(uint32_t)
      //  - Next 8 bytes are for the value(int64_t)
      auto const integer_value =
        dynamic_pointer_cast<json_values::integer_value>( value );
      // tag
      auto const tag = encoding_tag::Integer;
      outfile.write( reinterpret_cast<const char*>( &tag ), 1 );
      // Length
      auto const length = sizeof( numeric_key ) + sizeof( int64_t );
      outfile.write( reinterpret_cast<const char*>( &length ), 1 );
      // Value
      //  - Key
      outfile.write(
        reinterpret_cast<const char*>( &key ), sizeof( numeric_key ) );
      //  - Value
      outfile.write( reinterpret_cast<const char*>( &integer_value->value ),
        sizeof( int64_t ) );
      BOOST_LOG_TRIVIAL( debug )
        << "Integer written to file: tag=" << encoding_tag::Integer
        << ", length=" << length << ", key=" << key
        << ", value=" << integer_value->value;
      break;
    }
    case json_values::type::String:
    {
      // Encode the String as it follows:
      //  - first byte is the tag
      //  - Next 4 bytes are for the length, due to being a string we can't have
      //   only 1 byte for it(key + string length).
      //  - Next 4 bytes are for the key(uint32_t)
      //  - Next length - 4 bytes are for the string
      auto const string_value =
        dynamic_pointer_cast<json_values::string_value>( value );
      // tag
      auto const tag = encoding_tag::String;
      outfile.write(
        reinterpret_cast<const char*>( &tag ), sizeof( encoding_tag ) );
      // Length
      uint32_t const length =
        sizeof( numeric_key ) + string_value->value.size();
      outfile.write(
        reinterpret_cast<const char*>( &length ), sizeof( uint32_t ) );
      // Value
      //  - Key
      outfile.write(
        reinterpret_cast<const char*>( &key ), sizeof( numeric_key ) );
      //  - Value
      outfile.write( string_value->value.data(), string_value->value.size() );
      BOOST_LOG_TRIVIAL( debug )
        << "String written to file: tag=" << encoding_tag::String
        << ", length=" << length << ", key=" << key
        << ", value=" << string_value->value;
      break;
    }
    }
  }
  // This is now we will determine when we processed all keys from a JSON object
  auto const tag = encoding_tag::EntryEnd;
  outfile.write(
    reinterpret_cast<const char*>( &tag ), sizeof( encoding_tag ) );
  outfile.close();
  return true;
}
//----------------------------------------------------------------------------//
bool tlv_encoder::write_keys_map_to_file(
  keys_map& keys, const string& output_file_name )
{
  BOOST_LOG_TRIVIAL( trace ) << "tlv_encoder::write_keys_map_to_file called";
  error_message = "";
  if ( keys.empty() )
  {
    error_message = "Empty map";
    return false;
  }
  ofstream outfile;
  outfile.open( output_file_name, ios::binary | ios::out | ios::app );
  if ( !outfile.is_open() )
  {
    error_message =
      "Cant open file to write TLV encoded json: " + output_file_name;
    return false;
  }

  for ( auto const& [ key, value ] : keys )
  {
    // We'll encode the Key as it follows:
    //  - first byte is the tag
    //  - Next 4 bytes are for the length, due to the key being a string we
    //  can't have only 1 byte for it(key length, which is a string + value,
    //  which is a uint32).
    //  - Next 4 bytes are for the value(uint32_t)
    //  - Next length - 4 bytes are for the key string
    auto const tag = encoding_tag::Key;
    outfile.write(
      reinterpret_cast<const char*>( &tag ), sizeof( encoding_tag ) );
    // Length
    uint32_t const length = sizeof( numeric_key ) + key.size();
    outfile.write(
      reinterpret_cast<const char*>( &length ), sizeof( uint32_t ) );
    // Value
    //  - Key number
    outfile.write(
      reinterpret_cast<const char*>( &value ), sizeof( numeric_key ) );
    //  - Value String
    outfile.write( key.data(), key.size() );
    BOOST_LOG_TRIVIAL( debug )
      << "Key written to file: tag=" << tag << ", length=" << length
      << ", key=" << key << ", value=" << value;
  }
  // This is now we will determine when we processed all keys from a JSON object
  auto const tag = encoding_tag::EntryEnd;
  outfile.write(
    reinterpret_cast<const char*>( &tag ), sizeof( encoding_tag ) );
  outfile.close();
  return true;
}
//----------------------------------------------------------------------------//
const string& tlv_encoder::get_error_message() const noexcept
{
  return error_message;
}
//----------------------------------------------------------------------------//
} // namespace json_packer
