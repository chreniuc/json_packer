#ifndef SRC_JSON_PACKER_TLV_ENCODER_HPP
#define SRC_JSON_PACKER_TLV_ENCODER_HPP

#include <string>

#include "json_packer/containers.hpp" // values_map, keys_map

namespace json_packer
{
/**
 * @brief The tlv_encoder class is used to encode the maps containing the values
 * and keys map that were parsed from the json input file.
 *
 * The values will be encoded as it follows:
 *  - tag - 1 byte - Which can be one of the values from the encoding_tag enum
 *  - length - 1 byte for Boolean and Integer or 4 bytes(uint32) for String.
 * This contains the length of the remaining bytes from this value, which
 * represents: key + value.
 *  - key - 4 bytes(uint32), this is the numeric key that has been allocated for
 * the string key of this value
 *  - value(generic: length - 4 bytes(key size)):
 *    - 1 byte for Boolean
 *    - 8 bytes for Integer(int64_t)
 *    - X bytes for String
 * The end of a json object will be marked with EntryEnd.
 *
 * The keys will be encoded as it follows:
 *  - tag - 1 byte - Which can be one of the following values from the
 * encoding_tag enum: Key and EntryEnd.
 *  - length - 4 bytes(uint32). This contains the length of the remaining bytes
 * from this key, which represents: key + string value.
 *  - key - 4 bytes(uint32), this is the numeric key that has been allocated for
 * the string key of this value
 *  - value(generic: length - 4 bytes(key size)) - Contains the string
 * representation of the numeric key. The end of a json object will be marked
 * with EntryEnd.
 */
class tlv_encoder
{
  /**
   * @brief error_message Contains the error message in case one of the write_*
   * methods returns false. To access this use the getter: get_error_message
   */
  ::std::string error_message;

public:
  /**
   * @brief The encoding_tag enum Supported tags.
   */
  enum encoding_tag : char
  {
    String = 0x01,
    Integer = 0x02,
    Boolean = 0x03,
    Key = 0x11,
    EntryEnd = 0x12,
  };

  tlv_encoder() = default;

  /**
   * @brief write_values_map_to_file Writes the values in the format which was
   * explained above. It appends the values to the file. If errors are
   * encountered it will return false and populate the error message.
   * @param[in] values - Values map to be encoded.
   * @param[in] output_file_name - Name of the output file.
   * @return True or false, if false it will populate the error_message.
   */
  bool write_values_map_to_file(
    values_map& values, const ::std::string& output_file_name );

  /**
   * @brief write_keys_map_to_file Writes the keys in the format which was
   * explained above. It appends the values to the file. If errors are
   * encountered it will return false and populate the error message.
   * @param[in] keys - Keys map to be encoded.
   * @param[in] output_file_name - Name of the output file.
   * @return True or false, if false it will populate the error_message.
   */
  bool write_keys_map_to_file(
    keys_map& keys, const ::std::string& output_file_name );

  /**
   * @brief get_error_message Getter for the error message.
   * @return Error message
   */
  const ::std::string& get_error_message() const noexcept;
};
} // namespace json_packer
#endif // SRC_JSON_PACKER_TLV_ENCODER_HPP
