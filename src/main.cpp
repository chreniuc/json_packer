#include <cli.hpp>

#include <json_packer/packer.hpp>

using namespace std;

int main( int argc, char* argv[] )
{
  json_packer::cli cli_args;

  cli_args.initialize( argc, argv );
  cli_args.validate();

  json_packer::packer packer;

  if ( packer.pack_json( cli_args.input_json_file, cli_args.chunk_size,
         cli_args.persistent_keys ) )
  {
    return 0;
  }
  return 1;
}
