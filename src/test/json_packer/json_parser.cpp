// clang-format off
#define BOOST_TEST_MODULE json_packer_tests_tlv_encoder
#include <boost/test/included/unit_test.hpp>
// clang-format on
#include "test/json_packer/json_parser.hpp"

#include <json_packer/json_parser.hpp>
#include <json_packer/json_values/boolean_value.hpp>
#include <json_packer/json_values/integer_value.hpp>
#include <json_packer/json_values/string_value.hpp>

#include "test/utils.hpp"

using namespace std;

using parser = ::json_packer::json_parser;
using input_type = test::utils::input_type;
using json_type = ::json_packer::json_values::type;

namespace test::json_packer
{
size_t chunk_size{ 10 };
BOOST_FIXTURE_TEST_SUITE( suite_1, json_parser )
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ONE_LINE_JSON_VALID )
{
  string input_file{ "ONE_LINE_JSON_VALID.txt" };
  utils::generate_input_json_file(
    input_file, input_type::ONE_LINE_JSON_VALID );

  parser parser_obj( input_file, chunk_size );

  BOOST_TEST( parser_obj.parse_line( keys, values ) == true );
  BOOST_TEST( parser_obj.done() == true );
  BOOST_TEST( keys.size() == 3 );
  BOOST_TEST( values.size() == 3 );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ONE_LINE_JSON_VALID_WITH_LR )
{
  string input_file{ "ONE_LINE_JSON_VALID_WITH_LR.txt" };
  utils::generate_input_json_file(
    input_file, input_type::ONE_LINE_JSON_VALID_WITH_LR );

  parser parser_obj( input_file, chunk_size );

  BOOST_TEST( parser_obj.parse_line( keys, values ) == true );
  BOOST_TEST( parser_obj.done() == true );
  BOOST_TEST( keys.size() == 3 );

  BOOST_TEST( keys[ "key1" ] == 1 );
  BOOST_TEST( keys[ "key2" ] == 2 );
  BOOST_TEST( keys[ "key3" ] == 3 );

  BOOST_TEST( values.size() == 3 );
  auto const first_value = values[ 1 ];
  BOOST_TEST( first_value->get_type() == json_type::String );
  auto const first_string =
    dynamic_pointer_cast<::json_packer::json_values::string_value>(
      first_value );
  BOOST_TEST( first_string->value == "value1" );

  auto const second_value = values[ 2 ];
  BOOST_TEST( second_value->get_type() == json_type::Integer );
  auto const second_integer =
    dynamic_pointer_cast<::json_packer::json_values::integer_value>(
      second_value );
  BOOST_TEST( second_integer->value == 7 );

  auto const third_value = values[ 3 ];
  BOOST_TEST( third_value->get_type() == json_type::Boolean );
  auto const third_boolean =
    dynamic_pointer_cast<::json_packer::json_values::boolean_value>(
      third_value );
  BOOST_TEST( third_boolean->value == true );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TWO_LINES_JSON_VALID )
{
  string input_file{ "TWO_LINES_JSON_VALID.txt" };
  utils::generate_input_json_file(
    input_file, input_type::TWO_LINES_JSON_VALID );

  parser parser_obj( input_file, chunk_size );
  BOOST_TEST( parser_obj.parse_line( keys, values ) == true );
  BOOST_TEST( keys.size() == 3 );
  BOOST_TEST( values.size() == 3 );
  // Second line
  keys.clear();
  values.clear();
  BOOST_TEST( parser_obj.done() == false );
  BOOST_TEST( parser_obj.parse_line( keys, values ) == true );
  BOOST_TEST( keys.size() == 1 );
  BOOST_TEST( values.size() == 1 );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ONE_LINE_JSON_INVALID )
{
  string input_file{ "ONE_LINE_JSON_INVALID.txt" };
  utils::generate_input_json_file(
    input_file, input_type::ONE_LINE_JSON_INVALID );

  parser parser_obj( input_file, chunk_size );

  BOOST_TEST( parser_obj.parse_line( keys, values ) == false );
  BOOST_TEST( parser_obj.get_error_message() ==
    "Error while calling stream_parser.finish: incomplete JSON. We will ignore "
    "this line." );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_SUITE_END()
} // namespace test::json_packer
