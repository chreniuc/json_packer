#ifndef SRC_TEST_JSON_PACKER_JSON_PARSER_HPP
#define SRC_TEST_JSON_PACKER_JSON_PARSER_HPP

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

#include <json_packer/json_parser.hpp>

namespace test::json_packer
{
class json_parser
{
public:
  json_parser()
  {
    // Disable all logging
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::fatal );
  };

  ::json_packer::keys_map keys;
  ::json_packer::values_map values;
};
} // namespace test::json_packer
#endif // JSON_PARSER_HPP
