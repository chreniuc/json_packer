// clang-format off
#define BOOST_TEST_MODULE json_packer_tests_packer
#include <boost/test/included/unit_test.hpp>
// clang-format on

#include "test/json_packer/packer.hpp"
#include <json_packer/tlv_encoder.hpp>

#include <filesystem>

#include "test/utils.hpp"

using namespace std;

using encoding_tag = ::json_packer::tlv_encoder::encoding_tag;
using input_type = test::utils::input_type;

namespace test::json_packer
{
BOOST_FIXTURE_TEST_SUITE( suite_12, packer )
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TWO_LINES_PACKER_TEST )
{
  string input_file{ "TWO_LINES_PACKER_TEST.txt" };
  utils::generate_input_json_file(
    input_file, input_type::TWO_LINES_PACKER_TEST );
  string output_keys_file{ "TWO_LINES_PACKER_TEST.txt_keys_tlv_encoded" };
  string output_values_file{ "TWO_LINES_PACKER_TEST.txt_values_tlv_encoded" };
  utils::clean_file( output_keys_file );
  utils::clean_file( output_values_file );

  BOOST_TEST( packer_obj.pack_json( input_file, 10, false ) == true );

  // Check the values file
  ifstream values_file( output_values_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_values_file ) == 45 );
  // First entry
  // First value - String
  {
    encoding_tag tag;
    values_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::String );

    uint32_t length{ 0 };
    values_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 10 ); // 4 for the key number and 6 for the key string size

    ::json_packer::numeric_key key{ 0 };
    values_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    values_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "value1" );
  }
  // Second value - Integer
  {
    encoding_tag tag;
    values_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Integer );

    uint32_t length{ 0 };
    values_file.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( int64_t ) ); // 4 for the key number and 8 for value

    ::json_packer::numeric_key key{ 0 };
    values_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    int64_t value{ 0 };
    values_file.read( reinterpret_cast<char*>( &value ), sizeof( int64_t ) );
    BOOST_TEST( value == 2 );
  }
  // Third value - Boolean
  {
    encoding_tag tag;
    values_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Boolean );

    uint32_t length{ 0 };
    values_file.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( bool ) ); // 4 for the key number and 1 for value

    ::json_packer::numeric_key key{ 0 };
    values_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    bool value{ false };
    values_file.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
    BOOST_TEST( value == true );
  }

  encoding_tag end_entry_tag;
  values_file.read(
    reinterpret_cast<char*>( &end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( end_entry_tag == encoding_tag::EntryEnd );

  // Second line
  {
    // Third value - Boolean
    {
      encoding_tag tag;
      values_file.read(
        reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
      BOOST_TEST( tag == encoding_tag::Boolean );

      uint32_t length{ 0 };
      values_file.read( reinterpret_cast<char*>( &length ), 1 );
      BOOST_TEST( length ==
        sizeof( ::json_packer::numeric_key ) +
          sizeof( bool ) ); // 4 for the key number and 1 for value

      ::json_packer::numeric_key key{ 0 };
      values_file.read(
        reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
      BOOST_TEST( key == 1 );

      bool value{ false };
      values_file.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
      BOOST_TEST( value == false );
    }
  }
  encoding_tag values_end_entry_tag;
  values_file.read(
    reinterpret_cast<char*>( &values_end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( values_end_entry_tag == encoding_tag::EntryEnd );

  // -----------------------------------------------------------
  // Check the keys file
  // -----------------------------------------------------------
  ifstream keys_file( output_keys_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_keys_file ) == 54 );
  // First key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key1" );
  }
  // Second key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key2" );
  }
  // Third key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key3" );
  }
  encoding_tag keys_end_first_entry_tag;
  keys_file.read( reinterpret_cast<char*>( &keys_end_first_entry_tag ),
    sizeof( encoding_tag ) );
  BOOST_TEST( keys_end_first_entry_tag == encoding_tag::EntryEnd );
  // Second entry
  // First key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key1" );
  }
  encoding_tag keys_end_entry_tag;
  keys_file.read(
    reinterpret_cast<char*>( &keys_end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( keys_end_entry_tag == encoding_tag::EntryEnd );

  keys_file.close();
  values_file.close();
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TWO_LINES_PACKER_TEST_PERSISTENT_KEYS )
{
  string input_file{ "TWO_LINES_PACKER_TEST_PERSISTENT_KEYS.txt" };
  utils::generate_input_json_file(
    input_file, input_type::TWO_LINES_PACKER_TEST );
  string output_keys_file{ input_file + "_keys_tlv_encoded" };
  string output_values_file{ input_file + "_values_tlv_encoded" };
  utils::clean_file( output_keys_file );
  utils::clean_file( output_values_file );

  BOOST_TEST( packer_obj.pack_json( input_file, 10, true ) == true );

  // Check the values file
  ifstream values_file( output_values_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_values_file ) == 45 );
  // First entry
  // First value - String
  {
    encoding_tag tag;
    values_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::String );

    uint32_t length{ 0 };
    values_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 10 ); // 4 for the key number and 6 for the key string size

    ::json_packer::numeric_key key{ 0 };
    values_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    values_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "value1" );
  }
  // Second value - Integer
  {
    encoding_tag tag;
    values_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Integer );

    uint32_t length{ 0 };
    values_file.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( int64_t ) ); // 4 for the key number and 8 for value

    ::json_packer::numeric_key key{ 0 };
    values_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    int64_t value{ 0 };
    values_file.read( reinterpret_cast<char*>( &value ), sizeof( int64_t ) );
    BOOST_TEST( value == 2 );
  }
  // Third value - Boolean
  {
    encoding_tag tag;
    values_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Boolean );

    uint32_t length{ 0 };
    values_file.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( bool ) ); // 4 for the key number and 1 for value

    ::json_packer::numeric_key key{ 0 };
    values_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    bool value{ false };
    values_file.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
    BOOST_TEST( value == true );
  }

  encoding_tag end_entry_tag;
  values_file.read(
    reinterpret_cast<char*>( &end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( end_entry_tag == encoding_tag::EntryEnd );

  // Second line
  {
    // Third value - Boolean
    {
      encoding_tag tag;
      values_file.read(
        reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
      BOOST_TEST( tag == encoding_tag::Boolean );

      uint32_t length{ 0 };
      values_file.read( reinterpret_cast<char*>( &length ), 1 );
      BOOST_TEST( length ==
        sizeof( ::json_packer::numeric_key ) +
          sizeof( bool ) ); // 4 for the key number and 1 for value

      ::json_packer::numeric_key key{ 0 };
      values_file.read(
        reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
      BOOST_TEST( key == 1 );

      bool value{ false };
      values_file.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
      BOOST_TEST( value == false );
    }
  }
  encoding_tag values_end_entry_tag;
  values_file.read(
    reinterpret_cast<char*>( &values_end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( values_end_entry_tag == encoding_tag::EntryEnd );

  // -----------------------------------------------------------
  // Check the keys file
  // -----------------------------------------------------------
  ifstream keys_file( output_keys_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_keys_file ) == 40 );
  // First key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key1" );
  }
  // Second key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key2" );
  }
  // Third key
  {
    encoding_tag tag;
    keys_file.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    keys_file.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    keys_file.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    keys_file.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key3" );
  }
  encoding_tag keys_end_first_entry_tag;
  keys_file.read( reinterpret_cast<char*>( &keys_end_first_entry_tag ),
    sizeof( encoding_tag ) );
  BOOST_TEST( keys_end_first_entry_tag == encoding_tag::EntryEnd );
  // Try to read another byte, to trigger eof.
  keys_file.read( reinterpret_cast<char*>( &keys_end_first_entry_tag ),
    sizeof( encoding_tag ) );
  BOOST_TEST( keys_file.eof() == true );

  keys_file.close();
  values_file.close();
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_SUITE_END()
} // namespace test::json_packer
