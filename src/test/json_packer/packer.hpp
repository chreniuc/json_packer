#ifndef SRC_TEST_JSON_PACKER_PACKER_HPP
#define SRC_TEST_JSON_PACKER_PACKER_HPP

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

#include <json_packer/packer.hpp>

namespace test::json_packer
{
class packer
{
public:
  packer()
  {
    // Disable all logging
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::fatal );
  };

  ::json_packer::packer packer_obj;
};
} // namespace test::json_packer
#endif // SRC_TEST_JSON_PACKER_TLV_ENCODER_HPP
