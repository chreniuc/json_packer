// clang-format off
#define BOOST_TEST_MODULE json_packer_tests_tlv_encoder
#include <boost/test/included/unit_test.hpp>
// clang-format on

#include "test/json_packer/tlv_encoder.hpp"

#include <filesystem>

#include "test/utils.hpp"

using namespace std;

using encoding_tag = ::json_packer::tlv_encoder::encoding_tag;

namespace test::json_packer
{
BOOST_FIXTURE_TEST_SUITE( suite_12, tlv_encoder )
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( write_keys_map_to_file_EMPTY )
{
  string output_file{ "write_keys_map_to_file_EMPTY.bin" };
  BOOST_TEST( encoder.write_keys_map_to_file( keys, output_file ) == false );
  BOOST_TEST( encoder.get_error_message() == "Empty map" );
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( write_keys_map_to_file_ONE_ENTRY )
{
  string output_file{ "write_keys_map_to_file_ONE_ENTRY.bin" };
  utils::clean_file( output_file );
  utils::populate_keys_map( keys );
  BOOST_TEST( encoder.write_keys_map_to_file( keys, output_file ) == true );
  // Read from file
  ifstream inputfile( output_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_file ) == 27 );

  // First key
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key1" );
  }

  // Second key
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key2" );
  }

  inputfile.close();
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( write_keys_map_to_file_TWO_ENTRIES )
{
  string output_file{ "write_keys_map_to_file_TWO_ENTRIES.bin" };
  utils::clean_file( output_file );
  utils::populate_keys_map( keys );
  BOOST_TEST( encoder.write_keys_map_to_file( keys, output_file ) == true );
  BOOST_TEST( encoder.write_keys_map_to_file( keys, output_file ) == true );
  // Read from file
  ifstream inputfile( output_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_file ) == 54 );

  // First entry
  // First key
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key1" );
  }
  // Second key
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key2" );
  }

  encoding_tag end_entry_tag;
  inputfile.read(
    reinterpret_cast<char*>( &end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( end_entry_tag == encoding_tag::EntryEnd );

  // Second entry
  // First key
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key1" );
  }
  // Second key
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Key );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 8 ); // 4 for the key number and 4 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "key2" );
  }

  inputfile.close();
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( write_values_map_to_file_ONE_ENTRY )
{
  string output_file{ "write_values_map_to_file_ONE_ENTRY.bin" };
  utils::clean_file( output_file );
  utils::populate_values_map( values );
  BOOST_TEST( encoder.write_values_map_to_file( values, output_file ) == true );
  // Read from file
  ifstream inputfile( output_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_file ) == 37 );

  // First value - String
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::String );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 10 ); // 4 for the key number and 6 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "value1" );
  }

  // Second value - Integer
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Integer );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( int64_t ) ); // 4 for the key number and 8 for value

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    int64_t value{ 0 };
    inputfile.read( reinterpret_cast<char*>( &value ), sizeof( int64_t ) );
    BOOST_TEST( value == 2 );
  }

  // Third value - Boolean
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Boolean );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( bool ) ); // 4 for the key number and 1 for value

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    bool value{ false };
    inputfile.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
    BOOST_TEST( value == true );
  }

  inputfile.close();
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( write_values_map_to_file_TWO_ENTRIES )
{
  string output_file{ "write_values_map_to_file_TWO_ENTRIES.bin" };
  utils::clean_file( output_file );
  utils::populate_values_map( values );
  // Write the values twice to the map
  BOOST_TEST( encoder.write_values_map_to_file( values, output_file ) == true );
  BOOST_TEST( encoder.write_values_map_to_file( values, output_file ) == true );
  // Read from file
  ifstream inputfile( output_file, ios::in | ios::binary );
  BOOST_TEST( filesystem::file_size( output_file ) == 74 );
  // First entry
  // First value - String
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::String );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 10 ); // 4 for the key number and 6 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "value1" );
  }
  // Second value - Integer
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Integer );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( int64_t ) ); // 4 for the key number and 8 for value

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    int64_t value{ 0 };
    inputfile.read( reinterpret_cast<char*>( &value ), sizeof( int64_t ) );
    BOOST_TEST( value == 2 );
  }
  // Third value - Boolean
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Boolean );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( bool ) ); // 4 for the key number and 1 for value

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    bool value{ false };
    inputfile.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
    BOOST_TEST( value == true );
  }

  encoding_tag end_entry_tag;
  inputfile.read(
    reinterpret_cast<char*>( &end_entry_tag ), sizeof( encoding_tag ) );
  BOOST_TEST( end_entry_tag == encoding_tag::EntryEnd );

  // Second entry
  // First value - String
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::String );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), sizeof( uint32_t ) );
    BOOST_TEST(
      length == 10 ); // 4 for the key number and 6 for the key string size

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 1 );

    const size_t chars_to_read = length - sizeof( ::json_packer::numeric_key );
    string value;
    value.resize( chars_to_read );
    inputfile.read( value.data(), chars_to_read );
    BOOST_TEST( value == "value1" );
  }
  // Second value - Integer
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Integer );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( int64_t ) ); // 4 for the key number and 8 for value

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 2 );

    int64_t value{ 0 };
    inputfile.read( reinterpret_cast<char*>( &value ), sizeof( int64_t ) );
    BOOST_TEST( value == 2 );
  }
  // Third value - Boolean
  {
    encoding_tag tag;
    inputfile.read( reinterpret_cast<char*>( &tag ), sizeof( encoding_tag ) );
    BOOST_TEST( tag == encoding_tag::Boolean );

    uint32_t length{ 0 };
    inputfile.read( reinterpret_cast<char*>( &length ), 1 );
    BOOST_TEST( length ==
      sizeof( ::json_packer::numeric_key ) +
        sizeof( bool ) ); // 4 for the key number and 1 for value

    ::json_packer::numeric_key key{ 0 };
    inputfile.read(
      reinterpret_cast<char*>( &key ), sizeof( ::json_packer::numeric_key ) );
    BOOST_TEST( key == 3 );

    bool value{ false };
    inputfile.read( reinterpret_cast<char*>( &value ), sizeof( bool ) );
    BOOST_TEST( value == true );
  }
  inputfile.close();
}
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_SUITE_END()
} // namespace test::json_packer
