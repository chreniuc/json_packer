#ifndef SRC_TEST_JSON_PACKER_TLV_ENCODER_HPP
#define SRC_TEST_JSON_PACKER_TLV_ENCODER_HPP

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

#include <json_packer/tlv_encoder.hpp>

namespace test::json_packer
{
class tlv_encoder
{
public:
  tlv_encoder()
  {
    // Disable all logging
    boost::log::core::get()->set_filter(
      boost::log::trivial::severity >= boost::log::trivial::fatal );
  };

  ::json_packer::keys_map keys;
  ::json_packer::values_map values;

  ::json_packer::tlv_encoder encoder;
};
} // namespace test::json_packer
#endif // SRC_TEST_JSON_PACKER_TLV_ENCODER_HPP
