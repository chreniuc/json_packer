#include "test/utils.hpp"

#include <fstream>
#include <memory>

#include <json_packer/json_values/boolean_value.hpp>
#include <json_packer/json_values/integer_value.hpp>
#include <json_packer/json_values/string_value.hpp>

using namespace std;

namespace test
{
//----------------------------------------------------------------------------//
void utils::generate_input_json_file(
  const std::string& filename, const input_type type )
{
  ofstream ofs( filename, ofstream::out );
  switch ( type )
  {
  case input_type::ONE_LINE_JSON_VALID:
  {
    ofs << R"({"key1":"value1","key2":7,"key3":true})";
    break;
  }
  case input_type::ONE_LINE_JSON_VALID_WITH_LR:
  {
    ofs << R"({"key1":"value1","key2":7,"key3":true}
)";
    break;
  }
  case input_type::TWO_LINES_JSON_VALID:
  {
    ofs << R"({"key1":"value1","key2":7,"key3":true}
{"key7": 6})";
    break;
  }
  case input_type::ONE_LINE_JSON_INVALID:
  {
    ofs << R"({"key1":"value1","key2":7,"key3":true)";
    break;
  }
  case input_type::TWO_LINES_PACKER_TEST:
  {
    ofs << R"({"key1":"value1","key2":2,"key3":true}
{"key1":false})";
    break;
  }
  }
  ofs.close();
}
//----------------------------------------------------------------------------//
void utils::populate_keys_map( json_packer::keys_map& keys )
{
  keys.insert( { "key1"s, 1 } );
  keys.insert( { "key2"s, 2 } );
}
//----------------------------------------------------------------------------//
void utils::populate_values_map( ::json_packer::values_map& values )
{
  values.insert(
    { 1, make_shared<::json_packer::json_values::string_value>( "value1" ) } );
  values.insert(
    { 2, make_shared<::json_packer::json_values::integer_value>( 2 ) } );
  values.insert(
    { 3, make_shared<::json_packer::json_values::boolean_value>( true ) } );
}
//----------------------------------------------------------------------------//
void test::utils::clean_file( const std::string& filename )
{
  ofstream ofs( filename, ofstream::out );
  ofs.close();
}
//----------------------------------------------------------------------------//
} // namespace test
