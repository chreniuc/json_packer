#ifndef SRC_TEST_UTILS_HPP
#define SRC_TEST_UTILS_HPP

#include <string>

#include <json_packer/containers.hpp>

namespace test
{
class utils
{
public:
  enum class input_type
  {
    ONE_LINE_JSON_VALID = 0,
    ONE_LINE_JSON_VALID_WITH_LR,
    TWO_LINES_JSON_VALID,
    ONE_LINE_JSON_INVALID,
    TWO_LINES_PACKER_TEST
  };

  /**
   * @brief generate_input_json_file Generate an input json file for our unit
   * tests for json parsing. Based on the enum type this method will generate a
   * file with json contents, which will be used in an unit test.
   * @param[in] filename - name of the file.
   * @param[in] type - enum type.
   */
  static void generate_input_json_file(
    const ::std::string& filename, const input_type type );

  /**
   * @brief populate_keys_map - Generates some keys and adds them to the map, so
   * we can use them when we test the tlv encoder.
   * @param[in,out] keys - keys map, will be populated.
   */
  static void populate_keys_map( ::json_packer::keys_map& keys );
  /**
   * @brief populate_values_map - Generates some values and adds them to the
   * map, so we can use them when we test the tlv encoder.
   * @param[in,out] values - values map, will be populated.
   */
  static void populate_values_map( ::json_packer::values_map& values );

  /**
   * @brief clean_file Will open the file and write nothing, we will need this
   * when we test the tlv encoding. As if we run multipole times the same unit
   * tests, they will write to the same file, so we should clean them each time.
   * @param[in] filename - File to be cleaned.
   */
  static void clean_file( const ::std::string& filename );
};
} // namespace test
#endif // UTILS_HPP
